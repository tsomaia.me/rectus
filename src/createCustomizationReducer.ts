import { MANY_SET, MANY_UPDATE, SET, UPDATE } from './actionTypes'
import deepMerge from './deepMerge'

export function createCustomizationReducer(defaultOptions: Record<string, any> = {}) {
  return (state = defaultOptions, action) => {
    switch (action.type) {
      case SET: {
        return {
          ...state,
          [action.payload.key]: action.payload.value,
        }
      }

      case MANY_SET: {
        return {
          ...state,
          ...action.payload,
        }
      }

      case UPDATE: {
        return deepMerge(state, {
          [action.payload.key]: action.payload.value,
        })
      }

      case MANY_UPDATE: {
        return deepMerge(state, action.payload)
      }

      default: {
        return state
      }
    }
  }
}

export default createCustomizationReducer
