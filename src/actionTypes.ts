export const SET = '@@RECTUS/SET'
export const MANY_SET = '@@RECTUS/MANY_SET'
export const UPDATE = '@@RECTUS/UPDATE'
export const MANY_UPDATE = '@@RECTUS/MANY_UPDATE'
