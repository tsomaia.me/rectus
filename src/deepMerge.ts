export function deepMerge(a: any, b: any): any {
  const result = {}

  if (typeof a !== 'object' || typeof b !== 'object' || a === null || b === null) {
    return b
  }

  for (let key of Object.keys(a)) {
    if (b.hasOwnProperty(key)) {
      result[key] = deepMerge(a[key], b[key])
    } else {
      result[key] = a[key]
    }
  }

  for (let key of Object.keys(b)) {
    if (a.hasOwnProperty(key)) {
      result[key] = deepMerge(a[key], b[key])
    } else {
      result[key] = b[key]
    }
  }

  return result
}

export default deepMerge
