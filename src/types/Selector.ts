export interface Selector {
  <S>(state: S): any
}

export default Selector
