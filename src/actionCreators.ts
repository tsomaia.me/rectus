import { MANY_SET, MANY_UPDATE, SET, UPDATE } from './actionTypes'

export function set(key: string, value: any) {
  return {
    type: SET,
    payload: {
      key: key,
      value: value,
    },
  }
}

export function update(key: string, value: any) {
  return {
    type: UPDATE,
    payload: {
      key: key,
      value: value,
    },
  }
}

export function setMany(options: Record<string, any>) {
  return {
    type: MANY_SET,
    payload: options,
  }
}

export function updateMany(options: Record<string, any>) {
  return {
    type: MANY_UPDATE,
    payload: options,
  }
}
