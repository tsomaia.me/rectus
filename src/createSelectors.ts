import { Selector } from './types'

export function createSelectors(selectOptionsState: Selector) {
  return {
    get: <S>(state: S, key: string) => selectOptionsState(state)[key],
    createSelector: (key: string) => <S>(state: S) => selectOptionsState(state)[key],
  }
}

export default createSelectors
