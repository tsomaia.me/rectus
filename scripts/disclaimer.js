const path                        = require('path')
const { readFile, readdir, stat } = require('fs').promises
const readFileOptions             = { encoding: 'utf-8' }

async function main(...flags) {
  const packageInfos = await findPackageInfos('node_modules')
  let result         = ''

  for (const { name, version, repository, licenses, licenseText, noticeText } of packageInfos) {
    if (result) {
      result += '\n\n\n--------------------------------------------------------------------------------------\n\n\n'
    }

    result += `This product may include the following software: ${name}`

    if (version != null) {
      result += `, version ${version}.`
    }

    if (repository != null) {
      result += `\nA source code of the software is available at: ${repository}.`
    }

    if (licenses != null && licenses.length > 0) {
      result += '\nThe software is available under the following'

      if (licenses.length === 1) {
        result += ' license'
      } else {
        result += ' licenses'
      }

      result += `: ${licenses.join(', ')}.`
    }

    if (licenseText && noticeText) {
      result += `\nBelow are license and notice associated with the software:`
    } else if (licenseText) {
      result += `\nBelow is a license associated with the software:`
    } else if (noticeText) {
      result += `\nBelow is a notice associated with the software:`
    }

    if (licenseText) {
      result += `\n\n${licenseText}`
    }

    if (noticeText) {
      result += `\n\n${noticeText}`
    }
  }

  console.log(result)
}

async function findPackageInfos(dir) {
  const entryNames = await readdir(dir)
  const infos      = []

  for (const entryName of entryNames) {
    if (entryName[0] === '.') {
      continue
    }

    if (entryName[0] === '@') {
      infos.push(...await findPackageInfos(path.join(dir, entryName)))
    } else {
      const packageInfo = await findPackageInfo(path.join(dir, entryName))
      infos.push(packageInfo)
    }
  }

  return infos
}

async function findPackageInfo(dir) {
  const entryNames      = await readdir(dir)
  const rawPackageJson  = await readFile(path.join(dir, 'package.json'), readFileOptions)
  const packageJson     = rawPackageJson ? JSON.parse(rawPackageJson) : {
    name: path.basename(dir),
    version: null,
    license: null,
    repository: null,
    licenses: [],
  }
  const licenseFileName = await findLicenseFileName(dir, entryNames)
  const noticeFileName  = await findNoticeFileName(dir, entryNames)

  if (!packageJson.licenses) {
    packageJson.licenses = []
  }

  if (packageJson.license) {
    packageJson.licenses.unshift(packageJson.license)
  }

  return {
    name: packageJson.name,
    version: packageJson.version,
    licenses: packageJson.licenses,
    repository: resolveRepositoryUrl(packageJson.repository),
    licenseText: licenseFileName ? await readFile(licenseFileName, readFileOptions) : '',
    noticeText: noticeFileName ? await readFile(noticeFileName, readFileOptions) : '',
  }
}

function resolveRepositoryUrl(repository) {
  if (repository == null) {
    return null
  }

  let url = typeof repository === 'object'
              ? repository.url
              : repository

  if (url.startsWith('git+')) {
    url = url.substr(4)
  }

  return url
}

async function findLicenseFileName(dir, entryNames) {
  return await findFileName(
    dir,
    entryNames,
    isLicenseExactMatch,
    isLicenseApproximateMatch,
  )
}

async function findNoticeFileName(dir, entryNames) {
  return await findFileName(
    dir,
    entryNames,
    isNoticeExactMatch,
    isNoticeApproximateMatch,
  )
}

async function findFileName(dir, entryNames, isExactMatch, isApproximateMatch) {
  const exactMatch = entryNames.find(isExactMatch)

  if (exactMatch && await isFile(path.join(dir, exactMatch))) {
    return path.join(dir, exactMatch)
  }

  const approxMatch = entryNames.find(isApproximateMatch)

  if (approxMatch && await isFile(path.join(dir, approxMatch))) {
    return path.join(dir, approxMatch)
  }

  return null
}

async function isFile(path) {
  const stats = await stat(path)

  return stats.isFile()
}

function isLicenseExactMatch(entryName) {
  return entryName === 'LICENSE' || entryName === 'LICENSE.txt'
}

function isLicenseApproximateMatch(entryName) {
  return entryName.toUpperCase().includes('LICENSE')
}

function isNoticeExactMatch(entryName) {
  return entryName === 'NOTICE' || entryName === 'NOTICE.txt'
}

function isNoticeApproximateMatch(entryName) {
  return entryName.toUpperCase().includes('NOTICE')
}

main(...process.argv.slice(2)).catch(console.error)
