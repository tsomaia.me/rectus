Simple and easy-to-use option manager for Redux applications.

### Set up

Create customization reducer using `createCustomizationReducer` function
and optionally pass initial options as an object and register it in a store.

### Usage

One of four actions can be dispatched to alter options state:

- `set(key, value)` action sets the given value for the given key.
- `update(key, value)` action updates the given value for the given key, using **deep merge** strategy.
- `setMany(options)` action merges the options to the existing options in a store, using **shallow merge** strategy.
- `updateMany(options)` action merges the options to the existing options in a store, using **deep merge** strategy.

`createSelectors` utility function takes base selector function
and returns an object containing the following properties:
- `get(state, key)` takes state, applies it to the **base selector** and extracts a property from its' result by the given `key`.
- `get(key)` takes key and returns memoized function which in turn takes a `state` and when called,
applies it to the **base selector** and extracts a property from its' result by the given `key`.

Additionally, a `deepMerge` function is also exported and can be used as follows:

`const mergedResult = deepMerge({ a: 1, b: { c: 2, d: 5 } }, { a: 2, b: { c: 3, e: 7 } })`

Results in `{ a: 2, b: { c: 3, d: 5, e: 7 } }`
